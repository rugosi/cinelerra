/*
 * CINELERRA
 * Copyright (C) 2016-2020 William Morrow
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef CHANNELINFO_INC
#define CHANNELINFO_INC

class ChanSearch;
class ChanSearchGUI;
class ChanSearchTitleText;
class ChanSearchInfoText;
class ChanSearchMatchCase;
class ChanSearchText;
class ChanSearchStart;
class ChanSearchCancel;
class ChanSearchList;
class ChannelStatus;
class ChannelProgress;
class ChannelPanel;
class TimeLineItem;
class TimeLine;
class ChannelDataItem;
class ChannelData;
class ChannelScroll;
class TimeLineScroll;
class ChannelEvent;
class ChannelEventLine;
class ChannelFrame;
class ChannelInfoGUI;
class ChannelInfoOK;
class ChannelInfoCancel;
class ChannelInfoCron;
class ChannelInfoPowerOff;
class ChannelInfoFind;
class ChannelInfoGUIBatches;
class ChannelDir;
class ChannelPath;
class ChannelStart;
class ChannelDuration;
class ChannelEarlyTime;
class ChannelLateTime;
class ChannelSource;
class ChannelNewBatch;
class ChannelDeleteBatch;
class ChannelClearBatch;
class ChannelInfo;
class ChannelThread;
class ChannelScan;

#endif
