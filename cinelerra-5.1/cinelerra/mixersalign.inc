/*
 * CINELERRA
 * Copyright (C) 2008 Adam Williams <broadcast at earthling dot net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#ifndef __MIXERSALIGN_INC__
#define __MIXERSALIGN_INC__

class MixersAlignMixer;
class MixersAlignMixers;
class MixersAlignMixerList;
class MixersAlignMTrack;
class MixersAlignMTracks;
class MixersAlignMTrackList;
class MixersAlignATrack;
class MixersAlignATracks;
class MixersAlignATrackList;
class MixersAlignReset;
class MixersAlignThread;
class MixersAlignMatch;
class MixersAlignMatchAll;
class MixersAlignNudgeTracks;
class MixersAlignNudgeSelected;
class MixersAlignCheckPoint;
class MixersAlignUndoEDLs;
class MixersAlignUndoItem;
class MixersAlignUndo;
class MixersAlignWindow;
class MixersAlignARender;
class MixersAlignScanPackage;
class MixersAlignScanClient;
class MixersAlignScanFarm;
class MixersAlignMatchFwdPackage;
class MixersAlignMatchFwdClient;
class MixersAlignMatchFwdFarm;
class MixersAlignMatchRevPackage;
class MixersAlignMatchRevClient;
class MixersAlignMatchRevFarm;
class MixersAlignTargetPackage;
class MixersAlignTargetClient;
class MixersAlignTarget;
class MixersAlign;

#endif
