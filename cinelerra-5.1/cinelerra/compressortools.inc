
/*
 * CINELERRA
 * Copyright (C) 2008-2019 Adam Williams <broadcast at earthling dot net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#ifndef __COMPRESSORTOOLS_INC__
#define __COMPRESSORTOOLS_INC__

class BandConfig;
class CompressorConfigBase;
class CompressorClientFrame;
class CompressorFreqFrame;
class CompressorGainFrame;
class CompressorEngine;
class CompressorCopy;
class CompressorPaste;
class CompressorClearGraph;
class CompressorPopup;

#define GRAPH_BG_COLOR		0x559977
#define GRAPH_BORDER1_COLOR	0xeeaa44
#define GRAPH_BORDER2_COLOR	0xeeaaff
#define GRAPH_GRID_COLOR	0xeeffcc
#define GRAPH_ACTIVE_COLOR	0x99cc77
#define GRAPH_INACTIVE_COLOR	0x666688

#endif
