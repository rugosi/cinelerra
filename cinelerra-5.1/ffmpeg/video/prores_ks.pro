mov prores_ks
# Container Apple QuickTime movie (.mov).
# Do not render audio as it will fail with current opts
profile=3
quant_mat=auto
# Option to make players believe that
# the original Apple engine was used
vendor=apl0
# cin_pix_fmt=yuv422p10le; yuva444p10le; gbrp10le
# The possible video profile values for the ProRes codec are:
# 5 = 4444xq
# 4 = 4444
# 3 = HQ (high quality)
# 2 = Standard
# 1 = LT (light)
# 0 = Proxy
