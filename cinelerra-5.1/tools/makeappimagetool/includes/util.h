// local includes
#include "assert.h"
#include "misc.h"

// import functions from misc module for convenience
namespace linuxdeploy {
    namespace util {
        using namespace misc;
        using namespace assert;
    }
}
