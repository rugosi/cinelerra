/*
 * CINELERRA
 * Copyright (C) 2020 William Morrow
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __MOTION_51_INC__
#define __MOTION_51_INC__

class Motion51Config;
class Motion51Main;
class Motion51ScanPackage;
class Motion51ScanUnit;
class Motion51Scan;

class Motion51SampleSteps;
class Motion51DrawVectors;
class Motion51TrackingFile;
class Motion51Limits;
class Motion51ResetConfig;
class Motion51ResetTracking;
class Motion51EnableTracking;
class Motion51Window;

#define TRACKING_FILE "/tmp/motion51"

#endif
